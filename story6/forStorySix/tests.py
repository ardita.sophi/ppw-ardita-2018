from selenium import webdriver
import unittest
import time
from django.test import Client 
from django.urls import resolve 
from django.http import HttpRequest
from .views import index, landing_page_content, profil
from .models import Todo 
from .forms import Todo_Form 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options


class story6FunctionalTest(unittest.TestCase): 
    def setUp(self):
        chrome_options = Options()
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options) 
        super(story6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit() 
        super(story6FunctionalTest, self).tearDown()
    
    def test_functional_test(self):
        self.selenium.get('https://arditasophi.herokuapp.com')
        header_text = self.selenium.find_element_by_name('status') ##!!
        self.send_keys('Coba coba')
        self.submit()
        self.assertIn('Coba coba', self.selenium.page_source)
        time.sleep(5)
        self.selenium.quit()

class story6UnitTest(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/forStorySix/') 
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_index_func(self): 
        found = resolve('/forStorySix/') 
        self.assertEqual(found.func, index)
    
    def test_model_can_create_status(self):
        new_activity = Todo.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw') 
        # Retrieving all available activity 
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_story_6_post_success_and_render_the_result(self):
        test = 'Anonymous' 
        response_post = Client().post('/forStorySix', {'content': Anonymous})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/lab-5/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story_6_post_error_and_render_the_result(self): 
        test = 'Anonymous' 
        response_post = Client().post('/lab-5/add_todo', {'content': ''})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/lab-5/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

if __name__ == '__main__':
    unittest.main(warnings='ignore')