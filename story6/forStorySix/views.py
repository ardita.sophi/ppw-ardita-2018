from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect 
from .forms import Todo_Form 
from .models import Todo 

landing_page_content = 'Hello, Apa Kabar?'
def index(request):
    todo = Todo.objects.all()
    if request.method == 'POST':
        todo_form = Todo_Form(request.POST)
        if todo_form.is_valid():
            todo_form.save()
            return HttpResponseRedirect(reverse('index'))
    else:
        todo_form = Todo_Form()
    return render(request, 'landingpage.html', {'index': todo_form, 'yourStatus': todo, 'content': landing_page_content})
 
 def profil(request):
     return render(request, 'story.html', {})